package routes

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"api-heroku/controllers"
	"api-heroku/middlewares"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}

	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")

	r.Use(cors.New(corsConfig))

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)

	hotelsMiddlewareRoute := r.Group("/hotels")
	hotelsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/hotels", controllers.GetAllHotel)
	hotelsMiddlewareRoute.POST("", controllers.CreateHotel)
	r.GET("/hotels/:id", controllers.GetHotelById)
	hotelsMiddlewareRoute.PATCH("/:id", controllers.UpdateHotel)
	hotelsMiddlewareRoute.DELETE("/:id", controllers.DeleteHotel)

	photosMiddlewareRoute := r.Group("/photos")
	photosMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/photos", controllers.GetAllPhotos)
	photosMiddlewareRoute.POST("", controllers.CreatePhotos)
	photosMiddlewareRoute.PATCH("/:id", controllers.UpdatePhotos)
	r.GET("/photos/:id", controllers.GetPhotosById)
	photosMiddlewareRoute.DELETE("/:id", controllers.DeletePhotos)

	roomsMiddlewareRoute := r.Group("/rooms")
	roomsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	r.GET("/rooms", controllers.GetAllRoom)
	roomsMiddlewareRoute.POST("", controllers.CreateRoom)
	r.GET("/rooms/:id/hotels", controllers.GetHotelsByRoomId)
	roomsMiddlewareRoute.PATCH("/:id", controllers.UpdateRoom)
	r.GET("/rooms/:id", controllers.GetRoomlById)
	roomsMiddlewareRoute.DELETE("/:id", controllers.DeleteRoom)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
