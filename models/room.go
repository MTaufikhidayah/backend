package models

type Room struct {
	ID        uint    `gorm:"primary_key" json:"id"`
	Title     string  `json:"title"`
	Price     int     `json:"price"`
	MaxPeople int     `json:"maxPeople"`
	Desc      string  `json:"desc"`
	Type      string  `json:"type"`
	Hotel     []Hotel `json:"-"`
}
