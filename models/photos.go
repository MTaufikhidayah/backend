package models

type Photos struct {
	ID        uint   `gorm:"primary_key" json:"id"`
	Name      string `json:"name"`
	ImageUrl1 string `json:"imageUrl1"`
	ImageUrl2 string `json:"imageUrl2"`
	ImageUrl3 string `json:"imageUrl3"`
	ImageUrl4 string `json:"imageUrl4"`
	ImageUrl5 string `json:"imageUrl5"`
	ImageUrl6 string `json:"imageUrl6"`
	HotelID   uint   `json:"hotelId"`
}
