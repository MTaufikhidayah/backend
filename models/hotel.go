package models

type Hotel struct {
	ID            uint   `gorm:"primary_key" json:"id"`
	Title         string `json:"title"`
	Image         string `json:"image"`
	City          string `json:"city"`
	Address       string `json:"addres"`
	Desc          string `json:"desc"`
	Rating        int    `json:"rating"`
	Featured      string `json:"featured"`
	StartingPrice int    `json:"starttingPrice"`
	Room_ID       uint   `json:"roomsID"`
	Room          Room   `json:"-"`
	Photos        Photos `json:"photos"`
}
