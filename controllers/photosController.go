package controllers

import (
	"net/http"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type photosInput struct {
	Name      string `json:"name"`
	ImageUrl1 string `json:"imageUrl1"`
	ImageUrl2 string `json:"imageUrl2"`
	ImageUrl3 string `json:"imageUrl3"`
	ImageUrl4 string `json:"imageUrl4"`
	ImageUrl5 string `json:"imageUrl5"`
	ImageUrl6 string `json:"imageUrl6"`
	HotelID   uint   `json:"hotelId"`
}

// GetAllPhotos godoc
// @Summary Get all Photos.
// @Description Get a list of Photos.
// @Tags Photos
// @Produce json
// @Success 200 {object} []models.Photos
// @Router /photos [get]
func GetAllPhotos(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var photos []models.Photos
	db.Find(&photos)

	c.JSON(http.StatusOK, gin.H{"data": photos})
}

// CreatePhotos godoc
// @Summary Create New Photos.
// @Description Creating a new Photos.
// @Tags Photos
// @Param Body body photosInput true "the body to create a new Photos"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Photos
// @Router /photos [post]
func CreatePhotos(c *gin.Context) {
	// Validate input
	var input photosInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Photos
	photos := models.Photos{
		Name:      input.Name,
		ImageUrl1: input.ImageUrl1,
		ImageUrl2: input.ImageUrl2,
		ImageUrl3: input.ImageUrl3,
		ImageUrl4: input.ImageUrl4,
		ImageUrl5: input.ImageUrl5,
		ImageUrl6: input.ImageUrl6,
		HotelID:   input.HotelID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&photos)

	c.JSON(http.StatusOK, gin.H{"data": photos})
}

// GetPhotosById godoc
// @Summary Get Photos.
// @Description Get an Photos by id.
// @Tags Photos
// @Produce json
// @Param id path string true "Photos id"
// @Success 200 {object} models.Photos
// @Router /photos/{id} [get]
func GetPhotosById(c *gin.Context) { // Get model if exist
	var photos models.Photos

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&photos).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": photos})
}

// UpdatePhotos godoc
// @Summary Update Photos.
// @Description Update Photos by id.
// @Tags Photos
// @Produce json
// @Param id path string true "Photos id"
// @Param Body body photosInput true "the body to update age photos"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Photos
// @Router /photos/{id} [patch]
func UpdatePhotos(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var photos models.Photos
	if err := db.Where("id = ?", c.Param("id")).First(&photos).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input photosInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Photos
	updatedInput.Name = input.Name
	updatedInput.ImageUrl1 = input.ImageUrl1
	updatedInput.ImageUrl2 = input.ImageUrl2
	updatedInput.ImageUrl3 = input.ImageUrl3
	updatedInput.ImageUrl4 = input.ImageUrl4
	updatedInput.ImageUrl5 = input.ImageUrl5
	updatedInput.ImageUrl6 = input.ImageUrl6
	updatedInput.HotelID = input.HotelID

	db.Model(&photos).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": photos})
}

// DeletePhotos godoc
// @Summary Delete one Photos.
// @Description Delete a Photos by id.
// @Tags Photos
// @Produce json
// @Param id path string true "Photos id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /photos/{id} [delete]
func DeletePhotos(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var photos models.Photos
	if err := db.Where("id = ?", c.Param("id")).First(&photos).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&photos)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
