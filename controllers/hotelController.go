package controllers

import (
	"net/http"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type hotelInput struct {
	Title         string `json:"title"`
	Image         string `json:"image"`
	City          string `json:"city"`
	Address       string `json:"addres"`
	Desc          string `json:"desc"`
	Rating        int    `json:"rating"`
	Featured      string `json:"featured"`
	StartingPrice int    `json:"starttingPrice"`
	Room_ID       uint   `json:"roomsID"`
}

// GetAllHotels godoc
// @Summary Get all hotels.
// @Description Get a list of Hotels.
// @Tags Hotel
// @Produce json
// @Success 200 {object} []models.Hotel
// @Router /hotels [get]
func GetAllHotel(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var hotels []models.Hotel
	db.Find(&hotels)

	c.JSON(http.StatusOK, gin.H{"data": hotels})
}

// CreateHotel godoc
// @Summary Create New Hotel.
// @Description Creating a new Hotel.
// @Tags Hotel
// @Param Body body hotelInput true "the body to create a new hotel"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Hotel
// @Router /hotels [post]
func CreateHotel(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Validate input
	var input hotelInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Hotel
	hotel := models.Hotel{
		Title:         input.Title,
		Image:         input.Image,
		City:          input.City,
		Address:       input.Address,
		Desc:          input.Desc,
		Rating:        input.Rating,
		Featured:      input.Featured,
		StartingPrice: input.StartingPrice,
		Room_ID:       input.Room_ID,
	}
	db.Create(&hotel)

	c.JSON(http.StatusOK, gin.H{"data": hotel})
}

// GetHotelById godoc
// @Summary Get Hotel.
// @Description Get a Hotel by id.
// @Tags Hotel
// @Produce json
// @Param id path string true "hotel id"
// @Success 200 {object} models.Hotel
// @Router /hotels/{id} [get]
func GetHotelById(c *gin.Context) { // Get model if exist
	var hotel models.Hotel

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).Preload("Photos").Preload(clause.Associations).First(&hotel).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hotel})
}

// UpdateHotel godoc
// @Summary Update Hotel.
// @Description Update hotel by id.
// @Tags Hotel
// @Produce json
// @Param id path string true "hotel id"
// @Param Body body hotelInput true "the body to update an hotel"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Hotel
// @Router /hotels/{id} [patch]
func UpdateHotel(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var hotel models.Hotel
	var room models.Room
	if err := db.Where("id = ?", c.Param("id")).First(&hotel).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input hotelInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("id = ?", input.Room_ID).First(&room).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "roomID not found!"})
		return
	}

	var updatedInput models.Hotel
	updatedInput.Image = input.Image
	updatedInput.City = input.City
	updatedInput.Address = input.Address
	updatedInput.Title = input.Title
	updatedInput.Desc = input.Desc
	updatedInput.Rating = input.Rating
	updatedInput.Featured = input.Featured
	updatedInput.StartingPrice = input.StartingPrice
	updatedInput.Room_ID = input.Room_ID

	db.Model(&hotel).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": hotel})
}

// DeleteHotel godoc
// @Summary Delete one hotel.
// @Description Delete a hotel by id.
// @Tags Hotel
// @Produce json
// @Param id path string true "hotel id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /hotel/{id} [delete]
func DeleteHotel(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var hotel models.Hotel
	if err := db.Where("id = ?", c.Param("id")).First(&hotel).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&hotel)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
