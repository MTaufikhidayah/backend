package controllers

import (
	"net/http"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type roomInput struct {
	Title     string `json:"title"`
	Price     int    `json:"price"`
	MaxPeople int    `json:"maxPeople"`
	Desc      string `json:"desc"`
	Type      string `json:"type"`
}

// GetAllRoom godoc
// @Summary Get all Room.
// @Description Get a list of Room.
// @Tags Room
// @Produce json
// @Success 200 {object} []models.Room
// @Router /rooms [get]
func GetAllRoom(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var room []models.Room
	db.Find(&room)

	c.JSON(http.StatusOK, gin.H{"data": room})
}

// CreateRoom godoc
// @Summary Create New Room.
// @Description Creating a new Room.
// @Tags Room
// @Param Body body roomInput true "the body to create a new Room"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Room
// @Router /rooms [post]
func CreateRoom(c *gin.Context) {
	// Validate input
	var input roomInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Room
	room := models.Room{
		Title:     input.Title,
		Price:     input.Price,
		MaxPeople: input.MaxPeople,
		Desc:      input.Desc,
		Type:      input.Type,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&room)

	c.JSON(http.StatusOK, gin.H{"data": room})
}

// GetRoomById godoc
// @Summary Get Room.
// @Description Get a Room by id.
// @Tags Room
// @Produce json
// @Param id path string true "room id"
// @Success 200 {object} models.Room
// @Router /rooms/{id} [get]
func GetRoomlById(c *gin.Context) { // Get model if exist
	var room models.Room

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&room).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": room})
}

// GetMovieByRoomId godoc
// @Summary GetHotel.
// @Description Get allHotel by RoomId.
// @Tags Room
// @Produce json
// @Param id path string true "Room id"
// @Success 200 {object} []models.Hotel
// @Router /rooms/{id}/hotel [get]
func GetHotelsByRoomId(c *gin.Context) { // Get model if exist
	var hotel []models.Hotel

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("room_id = ?", c.Param("id")).Find(&hotel).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hotel})
}

// UpdateRoom godoc
// @Summary Update Room.
// @Description Update Room by id.
// @Tags Room
// @Produce json
// @Param id path string true "Room id"
// @Param Body body roomInput true "the body to update age room"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Room
// @Router /rooms/{id} [patch]
func UpdateRoom(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var room models.Room

	if err := db.Where("id = ?", c.Param("id")).First(&room).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input roomInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Room
	updatedInput.Title = input.Title
	updatedInput.Price = input.Price
	updatedInput.MaxPeople = input.MaxPeople
	updatedInput.Desc = input.Desc
	updatedInput.Type = input.Type
	db.Model(&room).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": room})
}

// DeleteRoom godoc
// @Summary Delete one room.
// @Description Delete a room by id.
// @Tags Room
// @Produce json
// @Param id path string true "room id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /rooms/{id} [delete]
func DeleteRoom(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var room models.Room
	if err := db.Where("id = ?", c.Param("id")).First(&room).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&room)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
